package com.giaquino.amaysim.ui.account

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.giaquino.amaysim.R
import com.giaquino.amaysim.common.app.ViewModel.State
import com.giaquino.amaysim.common.app.ViewModelActivity
import com.giaquino.amaysim.databinding.AccountActivityBinding
import com.giaquino.amaysim.model.entity.Response

class AccountActivity : ViewModelActivity<AccountViewModel>() {

  private lateinit var binding : AccountActivityBinding

  companion object {
    val EXTRA_RESPONSE = "AccountActivity.EXTRA_RESPONSE"
  }

  override fun createViewModel(state: State?): AccountViewModel {
    state?.let {
      return AccountViewModel(state as AccountViewModelState)
    }
    return AccountViewModel(intent.getParcelableExtra<Response>(EXTRA_RESPONSE))
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.account_activity)
    binding.viewModel = viewModel
    setSupportActionBar(binding.includeToolbar.toolbar)
  }

  override fun onDestroy() {
    super.onDestroy()
    binding.unbind()
  }
}