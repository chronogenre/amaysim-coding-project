package com.giaquino.amaysim.ui.login

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import com.giaquino.amaysim.R
import com.giaquino.amaysim.common.app.ViewModel.State
import com.giaquino.amaysim.common.app.ViewModelActivity
import com.giaquino.amaysim.common.extensions.toast
import com.giaquino.amaysim.databinding.LoginActivityBinding
import com.giaquino.amaysim.model.MockAmaysimApi
import com.giaquino.amaysim.model.entity.Response
import com.giaquino.amaysim.ui.account.AccountActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class LoginActivity : ViewModelActivity<LoginViewModel>() {

  private lateinit var loginLayout: LoginActivityBinding

  private val disposables = CompositeDisposable()

  override fun createViewModel(state: State?) = LoginViewModel(MockAmaysimApi(applicationContext))

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    loginLayout = DataBindingUtil.setContentView(this, R.layout.login_activity)
    loginLayout.activity = this
    loginLayout.editAccountId.setText("2593177") // easy login for dummy data
    loginLayout.editAccountId.setOnEditorActionListener { _, actionId, _ ->
      if (actionId == EditorInfo.IME_ACTION_DONE) {
        onLoginClick()
        return@setOnEditorActionListener true
      }
      return@setOnEditorActionListener false
    }
    LoginAnimations.animateStart(loginLayout, 500)
  }

  override fun onDestroy() {
    super.onDestroy()
    disposables.clear()
  }

  fun onLoginClick() {
    disposables.add(viewModel.login(loginLayout.editAccountId.text.toString())
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            { response -> showAccount(response) },
            { error -> error.message?.let { toast(it) } }))
  }

  fun showAccount(response: Response) {
    val intent = Intent(this, AccountActivity::class.java)
        .putExtra(AccountActivity.EXTRA_RESPONSE, response)
    startActivity(intent)
    finish()
  }
}