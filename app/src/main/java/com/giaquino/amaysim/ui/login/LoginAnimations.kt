package com.giaquino.amaysim.ui.login

import android.view.View
import com.giaquino.amaysim.databinding.LoginActivityBinding

object LoginAnimations {

  fun animateStart(binding: LoginActivityBinding, delay: Long) {
    binding.run {
      editAccountId.visibility = View.GONE
      btnLogin.visibility = View.GONE
      root.postDelayed({
        editAccountId.visibility = View.VISIBLE
        btnLogin.visibility = View.VISIBLE
      }, delay)
    }
  }
}