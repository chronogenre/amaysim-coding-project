package com.giaquino.amaysim.ui.login

import com.giaquino.amaysim.common.app.ViewModel
import com.giaquino.amaysim.model.AmaysimApi

class LoginViewModel(private val api: AmaysimApi) : ViewModel() {

  fun login(accountId: String) = api.getAccountDetails(accountId)
}