package com.giaquino.amaysim.ui.account

import android.os.Parcel
import android.os.Parcelable
import com.giaquino.amaysim.common.app.ViewModel.State
import com.giaquino.amaysim.model.entity.Response

class AccountViewModelState : State, Parcelable {

  companion object {
    @JvmField val CREATOR = object : Parcelable.Creator<AccountViewModelState> {
      override fun createFromParcel(source: Parcel) = AccountViewModelState(source)
      override fun newArray(size: Int): Array<AccountViewModelState?> = arrayOfNulls(size)
    }
  }

  val response: Response

  constructor(viewModel: AccountViewModel) : super(viewModel) {
    response = viewModel.response
  }

  constructor(source: Parcel) : super(source) {
    response = source.readParcelable(Response::class.java.classLoader)
  }

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) {
    dest.writeParcelable(response, flags)
  }
}