package com.giaquino.amaysim.ui.account

import com.giaquino.amaysim.common.app.ViewModel
import com.giaquino.amaysim.model.entity.AccountAttributes
import com.giaquino.amaysim.model.entity.ProductAttributes
import com.giaquino.amaysim.model.entity.Response
import com.giaquino.amaysim.model.entity.ServiceAttributes
import com.giaquino.amaysim.model.entity.SubscriptionAttributes
import kotlin.LazyThreadSafetyMode.NONE

class AccountViewModel : ViewModel {

  lateinit var response: Response

  // assume for now that response data is fixed base on the collection.json

  val account: AccountAttributes by lazy(NONE) {
    AccountAttributes(response.data.id, response.data.attributes)
  }

  val service: ServiceAttributes by lazy(NONE) {
    val data = response.included.filter { it.type == "services" }.first()
    ServiceAttributes(data.id, data.attributes)
  }

  val subscription: SubscriptionAttributes by lazy(NONE) {
    val data = response.included.filter { it.type == "subscriptions" }.first()
    SubscriptionAttributes(data.id, data.attributes)
  }

  val product: ProductAttributes by lazy(NONE) {
    val data = response.included.filter { it.type == "products" }.first()
    ProductAttributes(data.id, data.attributes)
  }

  constructor(response: Response) {
    this.response = response
  }

  constructor(state: AccountViewModelState) {
    this.response = state.response
  }

  override val state: State get() = AccountViewModelState(this)
}