package com.giaquino.amaysim

import android.app.Application
import android.os.StrictMode

class AmaysimApplication : Application() {

  override fun onCreate() {
    super.onCreate()
    if (BuildConfig.DEBUG) {
      StrictMode.setVmPolicy(StrictMode.VmPolicy.Builder()
          .penaltyLog()
          .detectAll()
          .build())
      StrictMode.setThreadPolicy(StrictMode.ThreadPolicy.Builder()
          .penaltyFlashScreen()
          .detectAll()
          .build())
    }
  }
}