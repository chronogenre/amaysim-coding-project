package com.giaquino.amaysim.common.extensions

import android.os.Bundle
import kotlin.properties.ReadOnlyProperty
import kotlin.reflect.KProperty

/**
 * Maps a String value to a property using [key] to get value from [Map]
 *
 * @param key to use in getting value from [Map]
 */
fun <T> T.map(map: Map<String, String>, key: String): ReadOnlyProperty<T, String> {
  return object : ReadOnlyProperty<T, String> {
    override fun getValue(thisRef: T, property: KProperty<*>) = map[key]!!
  }
}

/**
 * Maps a String? value to a property using [key] to get value from [Map]
 *
 * @param key to use in getting value from [Map]
 */
fun <T> T.mapNullable(map: Map<String, String>, key: String): ReadOnlyProperty<T, String?> {
  return object : ReadOnlyProperty<T, String?> {
    override fun getValue(thisRef: T, property: KProperty<*>) = map[key]
  }
}

/**
 * Maps an Int value to a property using [key] to get value from [Map].
 *
 * This method use [java.lang.Integer.parseInt] for conversion.
 *
 * @param key to use in getting value from [Map]
 */
fun <T> T.mapInt(map: Map<String, String>, key: String): ReadOnlyProperty<T, Int> {
  return object : ReadOnlyProperty<T, Int> {
    private val value: Int by lazy { map[key]!!.toInt() }
    override fun getValue(thisRef: T, property: KProperty<*>) = value
  }
}

/**
 * Maps a Boolean value to a property using [key] to get value from [Map]
 *
 * This method use [java.lang.Boolean.parseBoolean] for conversion.
 *
 * @param key to use in getting value from [Map]
 */
fun <T> T.mapBoolean(map: Map<String, String>, key: String): ReadOnlyProperty<T, Boolean> {
  return object : ReadOnlyProperty<T, Boolean> {
    private val value: Boolean by lazy { map[key]!!.toBoolean() }
    override fun getValue(thisRef: T, property: KProperty<*>) = value
  }
}

/**
 * Converts a [Map] to a [Bundle], this method does not retain type
 * and will be lost on the conversion.
 */
fun Map<String, String>.toBundle() = Bundle(this.size).apply {
  forEach { putString(it.key, it.value) }
}