package com.giaquino.amaysim.common.app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

abstract class ViewModelActivity<T : ViewModel> : AppCompatActivity() {

  companion object {
    const val EXTRA_VIEW_MODEL_STATE = "ViewModelActivity.EXTRA_VIEW_MODEL_STATE"
  }

  lateinit var viewModel: T

  protected abstract fun createViewModel(state: ViewModel.State?): T

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)
    outState.putParcelable(EXTRA_VIEW_MODEL_STATE, viewModel.state)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    var state: ViewModel.State? = null

    savedInstanceState?.let {
      state = savedInstanceState.getParcelable<ViewModel.State>(EXTRA_VIEW_MODEL_STATE)
    }

    viewModel = createViewModel(state)
  }

  override fun onStart() {
    super.onStart()
    viewModel.onStart()
  }

  override fun onResume() {
    super.onResume()
    viewModel.onResume()
  }

  override fun onPause() {
    super.onPause()
    viewModel.onPause()
  }

  override fun onStop() {
    super.onStop()
    viewModel.onStop()
  }

  override fun onDestroy() {
    super.onDestroy()
    viewModel.onDestroy()
  }
}
