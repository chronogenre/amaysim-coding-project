package com.giaquino.amaysim.common.app

import android.os.Parcel
import android.os.Parcelable

open class ViewModel {

  open fun onStart() {}

  open fun onResume() {}

  open fun onPause() {}

  open fun onStop() {}

  open fun onDestroy() {}

  open val state get() = State(this)

  /**
   * Base class responsible for representing the current state for the ViewModel.
   */
  open class State : Parcelable {

    companion object {
      @JvmField val CREATOR = object : Parcelable.Creator<State> {
        override fun createFromParcel(source: Parcel) = State(source)
        override fun newArray(size: Int): Array<State?> = arrayOfNulls(size)
      }
    }

    constructor(viewModel: ViewModel)

    constructor (source: Parcel)

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) {}
  }
}
