package com.giaquino.amaysim.common.extensions

import android.os.Bundle

/**
 * Extension function to convert a [Bundle] to [Map]
 */
fun Bundle.toMap(): Map<String, String> {
  val map = mutableMapOf<String, String>()
  keySet().forEach { key ->
    getString(key)?.let { map.put(key, it) } // ignore null values
  }
  return map.toMap()
}