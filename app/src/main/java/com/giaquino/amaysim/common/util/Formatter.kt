package com.giaquino.amaysim.common.util

object Formatter {

  @JvmStatic fun formatCentsToDollar(cents: Int) = cents.toDouble() / 100.00

  /**
   * Format cents to dollar up to 2 decimal places
   */
  @JvmStatic fun formatCentsToDollarDisplay(cents: Int)
      = String.format("%.2f $", formatCentsToDollar(cents))

  @JvmStatic fun formatMbtoGb(mb: Int) = mb.toDouble() / 1000.00

  /**
   * Format MB to GB up to 2 decimal places
   */
  @JvmStatic fun formatMbtoGbDisplay(mb: Int) = String.format("%.2f GB", formatMbtoGb(mb))
}