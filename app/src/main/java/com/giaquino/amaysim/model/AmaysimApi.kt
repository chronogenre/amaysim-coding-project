package com.giaquino.amaysim.model

import com.giaquino.amaysim.model.entity.Response
import io.reactivex.Observable

interface AmaysimApi {
  fun getAccountDetails(accountId: String): Observable<Response>
}