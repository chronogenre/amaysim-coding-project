package com.giaquino.amaysim.model.entity

import com.giaquino.amaysim.common.extensions.map
import com.giaquino.amaysim.common.extensions.mapBoolean
import com.giaquino.amaysim.common.extensions.mapInt

/**
 * Mapping of attributes for each [Data.type] assuming that current specification is fixed.
 *
 * Note that null fields are removed on this classes.
 */
data class AccountAttributes(val id: String, val attributes: Map<String, String>) {
  val paymentType: String by map(attributes, "payment-type")
  val title: String by map(attributes, "title")
  val firstName: String by map(attributes, "first-name")
  val lastName: String by map(attributes, "last-name")
  val dateOfBirth: String by map(attributes, "date-of-birth")
  val contactNumber: String by map(attributes, "contact-number")
  val emailAddress: String by map(attributes, "email-address")
  val emailAddressVerified: Boolean by mapBoolean(attributes, "email-address-verified")
  val emailSubscriptionStatus: Boolean by mapBoolean(attributes, "email-subscription-status")
}

data class SubscriptionAttributes(val id: String, val attributes: Map<String, String>) {
  val includedDataBalance: Int by mapInt(attributes, "included-data-balance")
  val expiryDate: String by map(attributes, "expiry-date")
  val autoRenewal: Boolean by mapBoolean(attributes, "auto-renewal")
  val primarySubscription: Boolean by mapBoolean(attributes, "primary-subscription")
}

data class ProductAttributes(val id: String, val attributes: Map<String, String>) {
  val name: String by map(attributes, "name")
  val unlimitedText: Boolean by mapBoolean(attributes, "unlimited-text")
  val unlimitedTalk: Boolean by mapBoolean(attributes, "unlimited-talk")
  val unlimitedInternationalText: Boolean by mapBoolean(attributes, "unlimited-international-text")
  val unlimitedInternationalTalk: Boolean by mapBoolean(attributes, "unlimited-international-talk")
  val price: Int by mapInt(attributes, "price")
}

data class ServiceAttributes(val id: String, val attributes: Map<String, String>) {
  val msn: String by map(attributes, "msn")
  val credit: Int by mapInt(attributes, "credit")
  val creditExpiry: String by map(attributes, "credit-expiry")
  val dataUsageThreshold: Boolean by mapBoolean(attributes, "data-usage-threshold")
}

