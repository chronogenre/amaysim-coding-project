package com.giaquino.amaysim.model

import android.content.Context
import com.giaquino.amaysim.model.entity.Response
import com.google.gson.Gson
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

class MockAmaysimApi(context: Context) : AmaysimApi {

  val response: Response by lazy { // mem cache for now
    val data = context.assets.open("data.json").bufferedReader().use { it.readText() }
    Gson().fromJson(data, Response::class.java)
  }

  override fun getAccountDetails(accountId: String): Observable<Response> {
    return Observable.just(response)
        .doOnNext {
          if (it.data.id != accountId) {
            throw IllegalArgumentException("Invalid Account ID: $accountId")
          }
        }
  }
}