package com.giaquino.amaysim.model.entity

import android.os.Parcel
import android.os.Parcelable
import com.giaquino.amaysim.common.extensions.toBundle
import com.giaquino.amaysim.common.extensions.toMap
import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("type") val type: String,
    @SerializedName("id") val id: String,
    @SerializedName("attributes") val attributes: Map<String, String>
) : Parcelable {

  companion object {
    @JvmField val CREATOR = object : Parcelable.Creator<Data> {
      override fun createFromParcel(source: Parcel): Data = Data(source)
      override fun newArray(size: Int): Array<Data?> = arrayOfNulls(size)
    }
  }

  constructor(source: Parcel) : this(
      source.readString(),
      source.readString(),
      source.readBundle().toMap()
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) {
    dest.writeString(type)
    dest.writeString(id)
    dest.writeBundle(attributes.toBundle())
  }
}
