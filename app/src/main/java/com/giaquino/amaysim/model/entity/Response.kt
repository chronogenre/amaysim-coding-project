package com.giaquino.amaysim.model.entity

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Response(
    @SerializedName("data") val data: Data,
    @SerializedName("included") val included: List<Data>
) : Parcelable {

  companion object {
    @JvmField val CREATOR = object : Parcelable.Creator<Response> {
      override fun createFromParcel(source: Parcel): Response = Response(source)
      override fun newArray(size: Int): Array<Response?> = arrayOfNulls(size)
    }
  }

  constructor(source: Parcel) : this(
      source.readParcelable<Data>(Data::class.java.classLoader),
      source.createTypedArrayList(Data.CREATOR)
  )

  override fun describeContents() = 0

  override fun writeToParcel(dest: Parcel, flags: Int) {
    dest.writeParcelable(data, 0)
    dest.writeTypedList(included)
  }
}