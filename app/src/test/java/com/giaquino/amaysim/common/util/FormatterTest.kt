package com.giaquino.amaysim.common.util

import org.junit.Assert.assertEquals
import org.junit.Test

class FormatterTest {

  val cents = listOf<Pair<Int, Double>>(
      Pair(3990, 39.90),
      Pair(10, 0.10),
      Pair(5, 0.05),
      Pair(123124, 1231.24),
      Pair(261235123, 2612351.23)
  )

  val mbs = listOf<Pair<Int, Double>>(
      Pair(1, 0.00),
      Pair(10, 0.01),
      Pair(100, 0.10),
      Pair(1024, 1.02),
      Pair(5650, 5.65)
  )

  @Test fun formatCentsToDollar() {
    cents.forEach { (input, expected) ->
      assertEquals(expected, Formatter.formatCentsToDollar(input), 0.01)
    }
  }

  @Test fun formatMbtoGb() {
    mbs.forEach { (input, expected) ->
      assertEquals(expected, Formatter.formatMbtoGb(input), 0.01)
    }
  }
}