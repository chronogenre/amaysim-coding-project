package com.giaquino.amaysim.model.entity

import com.google.gson.Gson
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class ResponseTest {

  private lateinit var data: String
  private lateinit var gson: Gson

  @Before fun setup() {
    gson = Gson()
    data = javaClass.getResource("/data.json").openStream().bufferedReader().use { it.readText() }
  }

  @Test fun successfulParsingOfCorrectData() {
    val response = gson.fromJson(data, Response::class.java)
    assertEquals(response.data.type, "accounts")
    assertEquals(response.data.id, "2593177")
    assertTrue(response.included.isNotEmpty())
  }
}